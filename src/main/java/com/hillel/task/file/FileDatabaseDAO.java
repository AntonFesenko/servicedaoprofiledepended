package com.hillel.task.file;


import com.hillel.task.DAO;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;

public class FileDatabaseDAO implements DAO {

    private File file;
    private String filePath;

    @Override
    public void save(String value) {
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(
                    new FileWriter(filePath, true));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            writer.newLine();
            writer.write(value);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public String readAll() {
        String text = null;
        try {
            text = new String(Files.readAllBytes(file.toPath()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return text;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    @PostConstruct
    public void init() throws Exception {
        System.out.println("Initializing  Bean");
        if (filePath == null) {
            throw new IllegalArgumentException("You must specify the filePath property of " + FileDatabaseDAO.class);
        }
        this.file = new File(filePath);
        this.file.createNewFile();
        System.out.println("File exists: " + file.exists());
    }

    @PreDestroy
    public void destroy() {
        System.out.println("Destroying  Bean");
        if (!file.delete()) {
            System.err.println("ERROR: failed  to delete file.");
        }
        System.out.println("File exists: " + file.exists());
    }
}
