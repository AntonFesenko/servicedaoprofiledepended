package com.hillel.task.file;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("file")
public class FileDatabaseConfig {

    @Bean(initMethod = "init", destroyMethod = "destroy")
    FileDatabaseDAO dao() {

        FileDatabaseDAO fileDatabaseDAO = new FileDatabaseDAO();
        fileDatabaseDAO.setFilePath(System.getProperty("java.io.tmpdir") + System.getProperty("file.separator") + "test.txt");
        return fileDatabaseDAO;
    }
}
