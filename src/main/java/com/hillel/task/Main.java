package com.hillel.task;

import com.hillel.task.file.FileDatabaseConfig;
import com.hillel.task.memory.InMemoryDataBaseConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.GenericApplicationContext;

public class Main {
    public static void main(String[] args) {
        GenericApplicationContext ctx = new AnnotationConfigApplicationContext(
                FileDatabaseConfig.class,
                InMemoryDataBaseConfig.class,
                Service.class);
        Service service = ctx.getBean("service", Service.class);
        service.save("shhshshshsh");
        service.save("aaaaaaaaaaaaaaaa");
        System.out.println(service.readAll());
        ctx.close();
    }
}
