package com.hillel.task.memory;

import com.hillel.task.DAO;

import java.util.LinkedHashSet;

public class InMemoryDataBaseDAO implements DAO {
    private LinkedHashSet<String> allText = new LinkedHashSet<>();

    @Override
    public void save(String value) {
        allText.add(value);
    }

    @Override
    public String readAll() {
        StringBuilder sb = new StringBuilder();
        for (String text : allText) {
            sb.append(text+"\n");
        }
        return sb.toString();
    }
}

