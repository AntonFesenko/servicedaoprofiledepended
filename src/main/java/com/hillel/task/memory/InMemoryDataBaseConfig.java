package com.hillel.task.memory;

import com.hillel.task.DAO;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("memory")
public class InMemoryDataBaseConfig {
    @Bean
    public DAO dao(){return new InMemoryDataBaseDAO();}
}